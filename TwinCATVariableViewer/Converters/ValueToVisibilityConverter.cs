﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows;

namespace TwinCATVariableViewer.Converters
{
    /// <summary>
    /// 将布尔绑定转换为可见性：true->visible，false->hidden
    /// </summary>
    [ValueConversion(typeof(object), typeof(Visibility))]
    public class ValueToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return Visibility.Hidden;
            }

            var boolValue = System.Convert.ToBoolean(value);
            if (parameter?.ToString() == "Invert") boolValue = !boolValue;

            return boolValue ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
