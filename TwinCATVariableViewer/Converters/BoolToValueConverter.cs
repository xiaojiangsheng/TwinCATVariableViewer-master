﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TwinCATVariableViewer.Converters
{
    /// <summary>
    /// 将布尔绑定转换为int（0或1）
    /// </summary>
    [ValueConversion(typeof(object), typeof(int))]
    public class BoolToValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            bool _bool;
            try
            {

                _bool = (bool)System.Convert.ChangeType(value, typeof(bool));
            }
            catch (Exception)
            {
                _bool = false;
            }

            return _bool ? 1 : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
