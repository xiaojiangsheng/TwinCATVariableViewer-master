﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace TwinCATVariableViewer.Converters
{
    /// <summary>
    /// 将ListView项绑定转换为项索引int作为字符串
    /// </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class IndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ListViewItem item = (ListViewItem)value;
            if (item == null) return "";
            ListView listView = ItemsControl.ItemsControlFromItemContainer(item) as ListView;
            if (listView == null) return "";
            int index = listView.ItemContainerGenerator.IndexFromContainer(item);
            return index.ToString();

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
