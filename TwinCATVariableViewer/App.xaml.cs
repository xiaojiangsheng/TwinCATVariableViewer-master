﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TwinCATVariableViewer.Views;

namespace TwinCATVariableViewer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //WPF单例程序:检测进程名字，名字一样就不再启动。
            Process p = Process.GetCurrentProcess();
            //if (Process.GetProcessesByName(p.ProcessName).Count() > 1) GetProcessesByName返回的是数组Process[]
            if (Process.GetProcessesByName(p.ProcessName).Length > 1) //调试状态下的ProcessName和打包后exe运行的进程名不一样，多了后缀.vshost
            {
                MessageBox.Show("程序已经运行！");
                System.Environment.Exit(0);
                //this.Shutdown();//如果缺少此句代码,即使在后加一句return也会有问题：第一次启动没问题，第二次启动提示已有运行的程序。
                //但是当关掉第一个实例之后，便无法启动（提示已有运行的程序），因为后台还会运行Application的实例。
            }
            base.OnStartup(e);
        }

    }
}
