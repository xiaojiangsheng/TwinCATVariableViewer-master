﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using TwinCATVariableViewer.Mvvm;
using TwinCATVariableViewer.ViewModels;
using TwinCATVariableViewer.TwinCAT;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Threading;
using TwinCAT.TypeSystem;
using System.Windows.Media;
using TwinCAT.Ads;
using TwinCATVariableViewer.Views;
using System.Diagnostics;
using TwinCAT;
using System.Windows;
using TwinCAT.Ads.TypeSystem;
using TwinCATVariableViewer.Models;
using System.Diagnostics.Tracing;

namespace TwinCATVariableViewer.ViewModels
{
    public class MainViewModel : ViewModelBase
    {

        #region 字段
        //private readonly List<object> _symbolValues = new List<object>();
        private int[] _activePorts; // 活动端口

        private readonly DispatcherTimer _refreshDataTimer = new DispatcherTimer(DispatcherPriority.Render);
        private ScrollViewer _scrollViewer;

        private readonly List<PlcConnection> _plcConnections = new List<PlcConnection>();

        private int _activePlc; // 激活PLC数量
        private MainView _mainView;
        private StartView splashScreenView;
        #endregion

        #region 属性
        /// <summary>
        /// 主窗体加载事件指令
        /// </summary>
        public ICommand MianViewLoadedCommand { get; set; }
        /// <summary>
        /// 重新连接
        /// </summary>
        public ICommand ReconnectCommand { get; set; }
        /// <summary>
        /// 符号变量过滤
        /// </summary>
        public ICommand SymbolFiliterCommand { get; set; }
        private ObservableCollection<SymbolInfoModel> _symbolListViewItems = new ObservableCollection<SymbolInfoModel>();
        /// <summary>
        /// 符号列表子项集合
        /// </summary>
        public ObservableCollection<SymbolInfoModel> SymbolListViewItems
        {
            get { return _symbolListViewItems ?? (_symbolListViewItems = new ObservableCollection<SymbolInfoModel>()); }
            set { _symbolListViewItems = value; }
        }

        private bool _plcConnected;
        public bool PlcConnected
        {
            get { return _plcConnected; }
            set
            {
                if (_plcConnected != value)
                {
                    if (value)
                    {
                        _refreshDataTimer.IsEnabled = true;
                        _refreshDataTimer.Tick += RefreshDataTimerOnTick;
                    }
                    else
                    {
                        _refreshDataTimer.IsEnabled = false;
                        _refreshDataTimer.Tick -= RefreshDataTimerOnTick;
                    }
                }
                _plcConnected = value;
            }
        }

        private ObservableCollection<ComboBoxModel> _comboBoxCollection;
        /// <summary>
        /// 下拉框列表
        /// </summary>
        public ObservableCollection<ComboBoxModel> ComboBoxCollection
        {
            get
            {
                if (_comboBoxCollection == null)
                {
                    _comboBoxCollection = new ObservableCollection<ComboBoxModel>();
                }
                return _comboBoxCollection;
            }
            set { _comboBoxCollection = value; OnPropertyChanged("ComboBoxCollection"); }
        }
        private ComboBoxModel _comboBoxItem;
        /// <summary>
        /// 下拉框选中信息
        /// </summary>
        public ComboBoxModel ComboBoxItem
        {
            get { return _comboBoxItem; }
            set { _comboBoxItem = value; OnPropertyChanged("ComboBoxItem"); }
        }
        private string _dumpStatus = "";
        /// <summary>
        /// 输出状态
        /// </summary>
        public string DumpStatus
        {
            get { return _dumpStatus; }
            set { _dumpStatus = value; OnPropertyChanged("DumpStatus"); }
        }
        private Brush _dumpStatusForeground = new SolidColorBrush();
        /// <summary>
        /// 输出状态字体前景色
        /// </summary>
        public Brush DumpStatusForeground 
        {
            get { return _dumpStatusForeground; }
            set { _dumpStatusForeground = value; OnPropertyChanged("DumpStatusForeground"); }
        }
        /// <summary>
        /// 鼠标滚轮区域
        /// </summary>
        //public ScrollViewer ScrollViewer { get; set; }
        /// <summary>
        /// 文本框输入的变量名称
        /// </summary>
        public string FiliterSymbolName { get; set; }
        #endregion
        public MainViewModel()
        {
            #region 应用启动ShowDialog
            splashScreenView = new StartView();
            splashScreenView.ShowDialog();
            if (splashScreenView.DialogResult == true)
            {
                if (PlcConnected) splashScreenView.Close();
            }
            #endregion
            _refreshDataTimer.Interval = TimeSpan.FromMilliseconds(500); // 设定定时器刷新时间间隔
            MianViewLoadedCommand = new RelayCommand(MianViewLoaded);
            ReconnectCommand = new RelayCommand(Reconnect);
            SymbolFiliterCommand = new RelayCommand(SymbolFiliter);
        }
        private void MianViewLoaded(object obj)
        {
            _mainView = obj as MainView;
            ConnectPlc();
            // 通过可视化树获取SymbolListView对象
            Decorator border = VisualTreeHelper.GetChild(_mainView.SymbolListView, 0) as Decorator;
            if (border != null) _scrollViewer = border.Child as ScrollViewer;
        }
        /// <summary>
        /// 通过CallMethodAction向ViewModel传递EventArgs参数
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        public void EnterKeyUp(object obj,KeyEventArgs e)
        {
            if (e.Key == Key.Enter) PopulateListView(FiliterSymbolName);
        }
        private void Reconnect(object obj)
        {
            if (PlcConnected)return;
            ConnectPlc();
        }
        private void SymbolFiliter(object obj)
        {
            PopulateListView(FiliterSymbolName);
        }
        private void ConnectPlc()
        {
            _activePlc = 0;// 清零
            _activePorts = AutoGetActivePlcPorts($"{AmsNetId.Local}", 801);// 获取端口号，TC2-->801,TC3-->851
            _plcConnections.Clear();// 清空
            _comboBoxCollection.Clear();
            SymbolListViewItems?.Clear();// 清空变量监视列表
            foreach (int port in _activePorts)
            {
                PlcConnection plcCon = new PlcConnection(new AmsAddress($"{AmsNetId.Local}:{port}"));
                plcCon.PlcConnectionError += PlcOnPlcConnectionError;
                plcCon.Connect();
                if (plcCon.Connected)
                {
                    // 连接成功+订阅事件
                    plcCon.Connection.AdsStateChanged += PlcAdsStateChanged;
                    plcCon.Connection.ConnectionStateChanged += PlcOnConnectionStateChanged;
                    plcCon.Connection.AmsRouterNotification += PlcOnAmsRouterNotification;
                }
                // 将有效的PlcConnection对象实例添加到集合
                _plcConnections.Add(plcCon);
            }

            if (_plcConnections.Count == 0)
            {
                UpdateDumpStatus("未找到活动的PLC", Colors.Red);
                return;
            }

            if (_plcConnections[_activePlc].Connected)
            {
                UpdateDumpStatus("正在检索符号", Colors.GreenYellow);
                PopulateListView();
            }

            // 将检索到的端口号，填入下拉框
            if (_plcConnections.Count > 0)
            {
                foreach (int activePort in _activePorts)
                {
                    ComboBoxModel comboBoxModel = new ComboBoxModel();
                    comboBoxModel.Text = activePort.ToString();
                    comboBoxModel.Key = activePort.ToString();
                    _comboBoxCollection.Add(comboBoxModel);
                }
            }
            PlcConnected = _plcConnections[_activePlc].Connected;
        }
        /// <summary>
        /// 自动获取活动中的端口号
        /// </summary>
        /// <param name="amsIp">PLC AMS IP</param>
        /// <param name="startPort">起始端口号</param>
        /// <returns>具有活动端口的int数组</returns>
        private static int[] AutoGetActivePlcPorts(string amsIp, int startPort)
        {
            List<int> activePorts = new List<int>();
            int port = startPort;
            int unsuccessfulAttempts = 0; // 尝试次数
            while (true)
            {
                // 起始端口号自增，获取当前有效端口
                if (unsuccessfulAttempts >= 100 || activePorts.Count >= 100) break;

                using (AdsSession session = new AdsSession(new AmsAddress($"{amsIp}:{port}")))
                {
                    session.Connect();
                    try
                    {
                        StateInfo stateInfo = session.Connection.ReadState();
                        if (stateInfo.AdsState != AdsState.Invalid)
                        {
                            activePorts.Add(port);
                        }
                        else
                        {
                            unsuccessfulAttempts++;
                        }
                        session.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        unsuccessfulAttempts++;
                    }
                }
                port++;
            }
            // 返回端口号数组
            return activePorts.ToArray();
        }
        private void PlcOnPlcConnectionError(object sender, PlcConnectionErrorEventArgs e)
        {
            UpdateDumpStatus(e.Message, Colors.Red);
            PlcConnected = _plcConnections[_activePlc].Connected;
        }
        #region ADS 事件

        private void PlcAdsStateChanged(object sender, AdsStateChangedEventArgs e)
        {
            Debug.WriteLine($"ADS state changed: {e.State.AdsState}; Device state: {e.State.DeviceState}");
            DisplayPlcState(e.State.AdsState);
            PlcConnected = e.State.AdsState == AdsState.Run || e.State.AdsState == AdsState.Stop;
        }

        private void PlcOnConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            Debug.WriteLine($"Client connection state was {e.OldState} and is now {e.NewState} because of {e.Reason}");
            if (e.NewState == ConnectionState.Connected) return;
            PlcConnected = false;
            UpdateDumpStatus("PLC state: Disconnected", Colors.Red);
        }

        private void PlcOnAmsRouterNotification(object sender, AmsRouterNotificationEventArgs e)
        {
            Debug.WriteLine($"AMS router notification: {e.State}");
            if (e.State == AmsRouterState.Start) return;
            PlcConnected = false;
            UpdateDumpStatus("ADS router stopped", Colors.Red);
        }

        #endregion
        /// <summary>
        /// 更新PLC输出状态
        /// </summary>
        /// <param name="text">提示内容</param>
        /// <param name="fontColor">字体颜色</param>
        private void UpdateDumpStatus(string text, Color fontColor)
        {
            text = text.Length <= 120 ? text : $"{text.Substring(0, 120)}...";
            DumpStatus = text;
            // Brush属于UI线程元素，需要线程调度
            DumpStatusForeground.Dispatcher.Invoke(() => 
            {
                DumpStatusForeground = new SolidColorBrush(fontColor);
            });
        }
        /// <summary>
        /// 填到ListView控件
        /// </summary>
        /// <param name="filterName"></param>
        private void PopulateListView(string filterName = null)
        {
            SymbolListViewItems?.Clear();
            if (_plcConnections.Count == 0) return;

            foreach (ISymbol symbol in _plcConnections[_activePlc].SymbolList)
            {
                if (filterName == null || symbol.InstancePath.ToLower().Contains(filterName.ToLower()))
                {
                    SymbolListViewItems?.Add(new SymbolInfoModel
                    {
                        Path = symbol.InstancePath,
                        Type = symbol.TypeName,
                        Size = symbol.Size,
                        IndexGroup = ((IAdsSymbol)symbol).IndexGroup,
                        IndexOffset = ((IAdsSymbol)symbol).IndexOffset,
                        IsStatic = symbol.IsStatic,
                        CurrentValue = "等待..." // GetSymbolValue(symbol) // 启动需要很长时间才能加载变量
                    }); 
                }
            }
        }
        /// <summary>
        /// 显示PLC状态
        /// </summary>
        /// <param name="state"></param>
        private void DisplayPlcState(AdsState state)
        {
            switch (state)
            {
                case AdsState.Invalid:
                    UpdateDumpStatus("PLC state: Invalid", Colors.Red);
                    break;
                case AdsState.Idle:
                    UpdateDumpStatus("PLC state: Idle", Colors.Orange);
                    break;
                case AdsState.Reset:
                    UpdateDumpStatus("PLC state: Reset", Colors.Orange);
                    break;
                case AdsState.Init:
                    UpdateDumpStatus("PLC state: Init", Colors.Orange);
                    break;
                case AdsState.Start:
                    UpdateDumpStatus("PLC state: Start", Colors.Yellow);
                    break;
                case AdsState.Run:
                    UpdateDumpStatus("PLC state: Run", Colors.GreenYellow);
                    break;
                case AdsState.Stop:
                    UpdateDumpStatus("PLC state: Stop", Colors.Orange);
                    break;
                case AdsState.SaveConfig:
                    UpdateDumpStatus("PLC state: SaveConfig", Colors.Orange);
                    break;
                case AdsState.LoadConfig:
                    UpdateDumpStatus("PLC state: LoadConfig", Colors.Orange);
                    break;
                case AdsState.PowerFailure:
                    UpdateDumpStatus("PLC state: PowerFailure", Colors.Orange);
                    break;
                case AdsState.PowerGood:
                    UpdateDumpStatus("PLC state: PowerGood", Colors.Orange);
                    break;
                case AdsState.Error:
                    UpdateDumpStatus("PLC state: Error", Colors.Orange);
                    break;
                case AdsState.Shutdown:
                    UpdateDumpStatus("PLC state: Shutdown", Colors.Orange);
                    break;
                case AdsState.Suspend:
                    UpdateDumpStatus("PLC state: Suspend", Colors.Orange);
                    break;
                case AdsState.Resume:
                    UpdateDumpStatus("PLC state: Resume", Colors.Orange);
                    break;
                case AdsState.Config:
                    UpdateDumpStatus("PLC state: Config", Colors.Orange);
                    break;
                case AdsState.Reconfig:
                    UpdateDumpStatus("PLC state: Reconfig", Colors.Orange);
                    break;
                case AdsState.Stopping:
                    UpdateDumpStatus("PLC state: Stopping", Colors.Orange);
                    break;
                case AdsState.Incompatible:
                    UpdateDumpStatus("PLC state: Incompatible", Colors.Red);
                    break;
                case AdsState.Exception:
                    UpdateDumpStatus("PLC state: Exception", Colors.Red);
                    break;
                default:
                    Debug.WriteLine(state);
                    break;
            }
        }
        /// <summary>
        /// 滚轮刷新视图
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void RefreshDataTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (_scrollViewer == null) return;
            for (int i = 0; i < (int)_scrollViewer.ViewportHeight; i++)
            {
                SymbolInfoModel symbol = SymbolListViewItems[(int)_scrollViewer.VerticalOffset + i];
                SymbolListViewItems[(int)_scrollViewer.VerticalOffset + i].CurrentValue = PlcConnection.GetSymbolValue(symbol, _plcConnections[_activePlc].Connection);
            }
        }
    }
}
