﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows;
using TwinCATVariableViewer.Views;
using System.Windows.Input;
using System.Threading;
using System.Windows.Media;
using TwinCATVariableViewer.Helper;
using TwinCATVariableViewer.Models;
using TwinCATVariableViewer.Mvvm;
using System.Diagnostics;

namespace TwinCATVariableViewer.ViewModels
{
    class StartViewModel : ViewModelBase
    {
        Window _splashScreenView;
        public StartModel StartModel { get; set; } = new StartModel();
        public ICommand LoadedCommand { get; set; }
        public ICommand ClosedCommand { get; set; }
        public ICommand EnterCommand { get; set; }

        public bool IsSplashScreenViewDone { get; set; }

        // 刷新UI元素的委托
        private delegate void RefreshDelegate();
        public StartViewModel()
        {
            LoadedCommand = new RelayCommand(Loaded);
            ClosedCommand = new RelayCommand(Closed);
            EnterCommand = new RelayCommand(Enter);
        }

        private void Enter(object obj)
        {
            if (_splashScreenView != null)
            {
                _splashScreenView.DialogResult = true;
                _splashScreenView.Close();
            }
        }
        private void Loaded(object obj)
        {
            _splashScreenView = obj as Window;
            // 检查TwinCAT是否已经安装
            if (CheckTcLib.CheckLibrary("tcadsdll.dll"))
            {
                IsSplashScreenViewDone = false;
                StartModel.LoadingStatusLabel = "TwinCAT变量监控";
                IsSplashScreenViewDone = true;
            }
            else
            {
                StartModel.LoadingStatusLabel = "未安装TwinCAT，请安装后重试！";
                IsSplashScreenViewDone = false;
                Application.Current.Shutdown();
            }
        }
        private void Closed(object obj)
        {
            if (_splashScreenView != null)
            {
                _splashScreenView.DialogResult = false;
                _splashScreenView.Close();
            }
            Application.Current.Shutdown();
        }
    }
}
