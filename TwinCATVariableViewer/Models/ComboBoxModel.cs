﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinCATVariableViewer.Mvvm;

namespace TwinCATVariableViewer.Models
{
    public class ComboBoxModel : ViewModelBase
    {
        private String key;
        /// <summary>
        /// Key值
        /// </summary>
        public String Key
        {
            get { return key; }
            set { key = value; OnPropertyChanged(Key); }
        }

        private String text;
        /// <summary>
        /// Text值
        /// </summary>
        public String Text
        {
            get { return text; }
            set { text = value; OnPropertyChanged(Text); }
        }
    }
}
