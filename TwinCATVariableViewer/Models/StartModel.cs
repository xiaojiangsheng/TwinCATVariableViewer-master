﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinCATVariableViewer.Mvvm;

namespace TwinCATVariableViewer.Models
{
    public class StartModel : ViewModelBase
    {
		private string _LoadingStatusLabel = "TwinCAT 变量查看器";

		public string LoadingStatusLabel
        {
			get { return _LoadingStatusLabel; }
			set { _LoadingStatusLabel = value; OnPropertyChanged("LoadingStatusLabel"); }
		}
    }
}
