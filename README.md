# TwinCAT变量监视工具

#### 介绍
WPF MVVM模式基于https://github.com/fengxing1121/TwinCatVariableViewer 开源项目二次开发。

#### 软件架构
github开源项目采用WPF事件驱动模式，基于此重新整理程序结构并更改为MVVM模式。


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  下载并安装依赖项costura.fody和microsoft.xaml.behaviors.wpf在Nuget程序管理包内下载安装即可。
2.  编译完成后生成TwinCATVariableViewer.exe和TwinCATVariableViewer.exe.config
3.  双击直接运行TwinCATVariableViewer.exe，前提个人PC需要提前安装TwinCAT。
4.  TwinCATVariableViewer.exe已经使用costura.fody完成依赖项dll打包，单独运行即可。

#### 参与贡献

1.  Github开源项目https://github.com/fengxing1121/TwinCatVariableViewer。
